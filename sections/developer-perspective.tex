\chapter{\Finished{Developer's perspective}}

\section{\Finished{Application Programming Interface}}

The main point of contact of developer and software is the software's API, which for processing engines is a set of
methods exposed by given technology.

\subsection{\Finished{Abstraction level}}

The abstraction level is about how much control we have over the processing process and how much code we need to write
to perform a given operation. We can distinguish four main categories of APIs:
\begin{description}
\item[Processor-based]  application is created as a set of separated components, often called \emph{processors}.
    Each processor can be connected to other processors, from which he receives the data, and processors to whom he
    sends the data. All of the processors form a DAG (directed acyclic graph). This form of API is very verbose and
    powerful. Listing \ref{lst:proc-based-api} shows an example of processor API from \emph{Apache Gearpump}.
\item[DSL-based] application is created as a set of operations, often called \emph{flows}. Each operation is done
    on some representation of dataset and it returns the representation of the new dataset. This form of API is very
     concise and easy to use. Listing \ref{lst:dsl-based-api} shows an example of DSL API from \emph{Apache Spark}.
\item[Single-processor-based] - API similar to \emph{Processor-based} but the application consists of only one
\emph{processor} and more complicated flows have to be implemented as set of different applications.

\item[SQL-like] Some of the compared technologies provide also a SQL-like language that can operate on data. This
form of API has slightly different purpose and design that previously mentioned and is often build on top of main
functionality, so it will be discussed in separate section.

\end{description}

\begin{lstlisting}[language=scala, frame=single, caption=Example of processor-based API, label={lst:proc-based-api}]
// Processor that splits the message on whitespaces
class Split(taskContext : TaskContext, conf: UserConfig) extends Task(taskContext, conf) {
  import taskContext.output
  override def onNext(msg : Message) : Unit = {
    msg.msg.asInstanceOf[String].lines.foreach { line =>
      line.split(``[\\s]+'').filter(_.nonEmpty).foreach { msg =>
        output(new Message(msg, System.currentTimeMillis()))
      }
    }
  }
}

// Composing processors into graph
val sourceProcessor = DataSourceProcessor(source)
val sinkProcessor = DataSinkProcessor(new KafkaSink(outputTopic, props))
val splitProcessor = Processor[Split](1)
val sumProcessor = Processor[Sum](1)
import Graph._
val app = StreamApplication(``wordCount'', Graph[Processor[_ <: Task], Partitioner](
  sourceProcessor ~> 
  splitProcessor ~> 
  sumProcessor ~> 
  sinkProcessor), UserConfig.empty)
\end{lstlisting}

\begin{lstlisting}[language=scala, caption=Example of DSL-based API,frame=single,label={lst:dsl-based-api}]
val sc = new SparkContext(conf)
sc.textFile(args(0))
  .flatMap(_.split(' '))
  .map(t => (t,1))
  .reduceByKey(_+_)
  .sortBy(_._2, ascending = false, 1)
\end{lstlisting}

\paragraph{Comparison}\mbox{}\\

Table \ref{tab:dev-api-abst-level} shows the abstraction levels offered by different technologies. It is worth
mentioning that composable, DSL-based API for \emph{Apache Samza} is being worked on \cite{SAMZA-914}

\FloatBarrier
\begin{table}[h!]\
\begin{center}
\begin{tabular}{|r|r|} 
  \hline
  Technology & Api abstraction level \\
  \hline
  MapReduce  & Single-processor-based \\
  Spark      & DSL-based \\
  Flink      & DSL-based \\
  Samza      & Single-processor-based \\
  Storm      & Processor-based, DSL-based \\
  Heron      & Processor-based \\
  Apex       & Processor-based \\
  Kafka Streams & Processor-based, DSL-based \\
  Gearpump   & Processor-based, DSL-based \\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-api-abst-level}API abstraction level.}
\end{table}
\FloatBarrier

\subsection{\Finished{Language}}

In table \ref{tab:dev-api-lang} we can see what programming languages can be used with given technology. As we can see,
Java is supported by all of them. The second place is taken by Scala, which is supported by 3 of them. Only
\emph{Apache Spark} supports non-jvm languages out-of-the-box.

\FloatBarrier
\begin{table}[h!]\
\begin{center}
\begin{tabular}{|r|r|} 
  \hline
  Technology & Languages with officially supported API \\
  \hline
  MapReduce  & Java \\
  Spark      & Java, Scala, Python, R \\
  Flink      & Java, Scala \\
  Samza      & Java \\
  Storm      & Java \\
  Heron      & Java \\
  Apex       & Java \\
  Kafka Streams&  Java \\
  Gearpump   &  Java, Scala \\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-api-lang}APIs programming languages.}
\end{table}
\FloatBarrier

\subsection{\Finished{Type-safety}}

When writing an application for data processing it is critical to know what we are processing. This
information may come from two sources: programmer knowledge of the system or API. If it comes from programmer
knowledge only, it requires the explicit casts between the types. The API can provide the information about what
data types are being processed in particular part of the program with use of generic programming. Such API protect
the programmer from runtime casting errors and increase general software quality~\cite{Ray:2014:LSS:2635868.2635922}
~\cite{Prechelt:1998:CEA:279131.279140}.

Listing \ref{lst:typesafe-api} shows an example of strongly typed API from \emph{Apache Flink}, DataStream know what
type it contains, and after mapping it with function , it contains the same type as the output of the function :

\begin{lstlisting}[language=scala,label={lst:typesafe-api},caption=Example of type-safe API,frame=single]
class DataStream[T](stream: JavaStream[T]) {
  def map[R: TypeInformation](fun: T => R): DataStream[R] = { .. }
}
\end{lstlisting}

Listing \ref{lst:weak-api} shows an example of weakly typed API from \emph{Apache Storm}, there is no exact type
information about data being consumed or produced:
\begin{lstlisting}[language=scala,label={lst:weak-api},caption=Example of weakly-typed API,frame=single]
public interface IBasicBolt extends IComponent {
  void execute(Tuple input, BasicOutputCollector collector);
}
\end{lstlisting}

\paragraph{Comparison} \mbox{}

In table \ref{tab:dev-api-typesafety} we can see what APIs are offered by which technologies. Almost all DSL-based API
are typesafe (only Storm Trident exposes weakly-typed DSL api).

\FloatBarrier
\begin{table}[h!]\
\begin{center}
\begin{tabular}{|p{5cm}|p{8cm}|} 
  \hline
  Technology API & Typesafety level \\
  \hline
  MapReduce      & Strongly typed \\
  Spark          & Strongly typed \\
  Flink          & Strongly typed and weakly typed \\
  Samza          & Weakly typed \\
  Storm - processor API & Weakly typed \\
  Storm - DSL API  & Weakly typed \\
  Heron          & Weakly typed \\
  Apex           & Strongly typed \\
  Kafka Streams - processor API
                 & Strongly typed operators, weekly typed connections \\
  Kafka Streams - DSL API
                 &  Strongly typed \\
  Gearpump - processor API & Weakly typed \\
  Gearpump - DSL API & Strongly types  \\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-api-typesafety}Typesafety support of different technologies.}
\end{table}
\FloatBarrier

\subsection{\Finished{Higher level, external APIs}}
There are some projects, that provide another set of APIs over existing processing engines, so we are not limited to the APIs provided by core project. Examples:
\begin{itemize}
\item \textbf{Cascading} - Weakly typed processor-based Java API build on top of \emph{Apache MapReduce}. It also supports \emph{Apache Flink} and support for \emph{Apache Spark} is coming.
\item \textbf{Scalding} - Strongly typed DSL-based Scala API build on top of \emph{Cascading}
\item \textbf{Apache Beam} - Strongly typed DSL-based Java and Python API that unifies creating applications for \emph{Apache Spark}, \emph{Apache Flink}, \emph{Apache Apex}, \emph{Google Dataflow}
\item \textbf{Apache Crunch} - Strongly typed DSL-based Java ans Scala API running on top of \emph{MapReduce} and \textbf{Apache Spark}
\end{itemize}

\section{\Finished{Advanced features}}

\subsection{\Finished{Data windowing}}


Data windowing is the process of grouping events based on some notion of time. The time may come from different
sources  and the grouping process may take different forms.

Following classification of time widnows was described in \cite{warski-windowing}:

Based on time sources:
\begin{itemize}
\item{event-time} - time extracted from message itself
\item{ingestion-time} - time assigned to the event when it enters the system
\item{processing-time} - time when the windowing happens
\end{itemize}

Based on window type:
\begin{itemize}
\item{fixed} - non-overlapping windows of fixed length
\item{sliding} - overlapping windows of fixed length
\item{session} - windows start and end is calculated based on data inside messages
\end{itemize}


\paragraph{Apache Spark} \mbox{}
\FloatBarrier

\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Feature         & Support \\
  \hline
  Event time      & Supported only in Structured Streaming \\
  Ingestion time  & No info \\
  Processing time & Supported \\
\hline
  Fixed windows   & Supported \\
  Sliding Windows & Supported \\
  Session windows & Not supported\\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-windows-spark}Windowing features support for Apache Spark.}
\end{table}
\FloatBarrier

As we can see, \emph{Spark Streaming} can only support basic windowing basing on processing time\cite{spark-streaming-docs}, but event time
processing can be achieved with \emph{Spark Structured Streaming}~\cite{spark-struct-streaming}.


\paragraph{Apache Flink} \mbox{}
\FloatBarrier

\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Feature         & Support \\
  \hline
  Event time      & Supported \\
  Ingestion time  & Supported \\
  Processing time & Supported \\
\hline
  Fixed windows   & Supported \\
  Sliding Windows & Supported \\
  Session windows & Supported\\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-windows-flink}Windowing features support for Apache Flink.}
\end{table}
\FloatBarrier

\emph{Apache Flink} has very broad support for windowing and also it support customized \emph{Triggers}(condition
 under which window is processed) and \emph{Evictors}(rules that specify when an element can be removed). Also the
 user have the possibility to implement custom elements of almost every element taking part in data windowing (e.g.
 \emph{Window Asigner})\cite{flink-windows}.

\paragraph{Apache Samza} \mbox{}
\FloatBarrier

\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Feature         & Support \\
  \hline
  Event time      & Not Supported \\
  Ingestion time  & Not Supported \\
  Processing time & Supported \\
\hline
  Fixed windows   & Supported \\
  Sliding Windows & Not Supported \\
  Session windows & Not Supported\\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-windows-samza}Windowing features support for Apache Samza.}
\end{table}
\FloatBarrier

\emph{Apache Samza} support for windowing is very limited and consists only of fixed processing-time windows.

\paragraph{Apache Storm \& Twitter Heron} \mbox{}

These two technologies are described together, because \emph{Twitter Heron} is using \emph{Apache Storm} API as entrypoint.
Data based on  \cite{storm-windowing}.

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Feature         & Support \\
  \hline
  Event time      & Supported \\
  Ingestion time  & Can be implemented \\
  Processing time & Supported \\
\hline
  Fixed windows   & Supported \\
  Sliding Windows & Supported \\
  Session windows & Not Supported\\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-windows-storm}Windowing features support for Apache Storm.}
\end{table}
\FloatBarrier


\paragraph{Apache Apex} \mbox{}

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Feature         & Support \\
  \hline
  Event time      & Supported \\
  Ingestion time  & Supported \\
  Processing time & Supported \\
\hline
  Fixed windows   & Supported \\
  Sliding Windows & Supported \\
  Session windows & Supported\\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-windows-apex}Windowing features support for Apache Apex.}
\end{table}
\FloatBarrier

\emph{Apache Apex} support for windowing is very similar to the one provided by \emph{Apache Flink}. It's worth
noting, that thanks to Apex's API being very low level it allows even more customization than \emph{Apache Flink}(e
.g. specify state storage for windowed operators)\cite{apex-windowing}.


\paragraph{Kafka Streams} \mbox{}

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Feature         & Support \\
  \hline
  Event time      & Supported \\
  Ingestion time  & Supported \\
  Processing time & Supported \\
\hline
  Fixed windows   & Supported \\
  Sliding Windows & Supported \\
  Session windows & Supported\\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-windows-apex}Windowing features support for Apache Apex.}
\end{table}
\FloatBarrier

\emph{Kafka Streams} offers full support for windowing operations as well~\cite{kstreams-concepts}
\cite{kstreams-dev-guide}.


\paragraph{Apache Gearpump} \mbox{}

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Feature         & Support \\
  \hline
  Event time      & Not documented \\
  Ingestion time  & Not documented \\
  Processing time & Not documented \\
\hline
  Fixed windows   & Not documented \\
  Sliding Windows & Not documented \\
  Session windows & Not documented\\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-windows-gearpump}Windowing features support for Apache Gearpump.}
\end{table}
\FloatBarrier

Windowing is not mentioned anywhere in documentation of \emph{Apache Gearpump}, but we can see some classes related
to it in API specification (e.g. \emph{SessionWindow}). The support was implemented in \cite{GEARPUMP-23}  and has to
come in version 0.8.3.

% What triggers the processing of window?
% What we do with old values (acuumulation startegy) ?

\subsection{\Finished{Stateful computations}}

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{12cm}|}
  \hline
  Technology & Stateful computations handling \\
  \hline
  MapReduce  &  No mechanism available for global state handling,  it has to be expressed as part of the (key, value)
                pair. Local state can be a part of Mapper or Reducer instance. \\[10pt]

  Spark - batch     &  \emph{Broadcasts} for sharing immutable state and \emph{Accumulators} for global mutable state.
                    ~\cite{spark-prog-guide}
                    \\[10pt]
  Spark - streaming & Special methods \emph{updateStateByKey} and \emph{mapWithState} available
                     \cite{spark-streaming-docs}.
                     \\[10pt]
  Flink - batch     &  \emph{Broadcast variables} and \emph{Distributed Cache} \cite{flink-dset-prog-guide}
                    \\[10pt]
  Flink - streaming & \emph{Operator State} which is bound to particular operator and \emph{Keyed State} which is
                         bound to the operator and message key\cite{flink-state}.
                    \\[10pt]
  Samza             &  State is stored on disk, on every node and replicated through separated kafka stream (as a
                        changelog). Different storage handler can be used, the default one is a key-value store
                        based on \emph{LevelDb}~\cite{samza-state}.
                    \\[10pt]
  Storm Core \& Heron & Integrated support for key value store, with pluggable backends (in-memory or redis). Other
                        kinds of state may be used by implemented \emph{State} interface. State operations have
                        at-least-once guarantee, so in case of failure, data in state backend may be duplicated.
                        \cite{storm-state}
                    \\[10pt]
  Storm Trident     &  Trident provides a sophisticated API for handling state regarding the transacionality of the
                       processing. Many different backends are provided, including: Hive, HBase, HDFS, Cassandra,
                       Memcache, Redis and others.
                       \cite{trident-state}
                    \\[10pt]
  Apex              & No dedicated state API is provided, but all of the operators fields which are not market as
                    transient are considered to be a state and take part in checkpointing process. State backend is called
                    \emph{StorageAgent}s and by default two implementations are provided out of the box (in-memory,
                    which is saved to hdfs on checkpoints and File system based with in-memory cache.
                    \\[10pt]
  Kafka Streams     & Mechanism called \emph{State Stores} is provided, which exposes the api of key-value store. Under
                        the hood the stores are replicated through kafka changelog stream\cite{kstreams-dev-guide}.
                    \\[10pt]
  Gearpump          & Not documented.
                    \\[10pt]
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-state}Statefull computations support in different technologies.}
\end{table}
\FloatBarrier

As we can see, most of the technologies provide some way of handling stateful computations. Many of them allow
to switch the backing implementation. Also most of them isolate the state of different data partitions.

\subsection{\Finished{Dynamic scalability}}

Dynamic scalability is the ability to increase or decrease the number of resources used by application without
losing any state/progress. It is useful in example go handling data peaks. Such mechanism can be either manual or
automatic (e.g.
based on latency).

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{12cm}|}
  \hline
  Technology & Dynamic scalability handling \\
  \hline
  MapReduce & Not supported. Number of mappers and reducers is being set at the moment of submitting job to YARN and is
                calculated based o n many different factors, including configuration hints, scheduling policies,
                resources availability, etc.
                \\[10pt]
  Spark     & Dynamic scalability is handled by mechanism called \emph{dynamic allocation}. It allows to automatically
                increase and reduce the number of executors\cite{spark-config}.
                \\[10pt]
  Flink     & The dynamic scalability has been introduced in Flink 1.2.0 and allows to start and application from
                checkpoint with different parallelism than it was running previously\cite{flink-1.2-rnotes}
                \\[10pt]
  Samza     & Auto scaling has been mentioned in \cite{SAMZA-336}, but the issue is not resolved. Manual scaling is
                supported: user has to define number of containers during the startup (via \emph{job.container.count}
                setting), the minimum is one and the maximum is number of kafka topic partitions. Application can be
                safely restarted with different number of containers without losing any data.
                \\[10pt]
  Storm     & Autoscaling is scope of \cite{STORM-594} issue an is unresolved at this moment. Storm allows to rebalance
                topology, which means that user can in any moment change the number of workers (processes) or
                executors (threads). This can be done either via web UI or CLI. The only limitation is number of tasks,
                which has to be set up at the beginning of topology life and cannot be changed later (number of
                executors has to be lower or equal to number of tasks, because each executor can run one or more
                tasks)\cite{storm-parallelism}.
                \\[10pt]
  Apex      &  Documentation claims that Apex provides very rich capabilities of live modification of applications,
                including not only number of resources but also internal operator configuration\cite{apex-app-development}. No
                direct examples could be found.
                \\[10pt]
  Kafka Streams & User can scale processing horizontally by launching new instances of the application. Effective
                number of instances is limited by number of topic partitions\cite{kstreams-home}.
                \\[10pt]
  Gearpump  & Not documented
                \\[10pt]
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-dyn-scale}Dynamic scalability support in different technologies.}
\end{table}
\FloatBarrier

Many of the technologies provides the ability to dynamically scale the processing, but only \emph{Apache Spark}
supports automatic changes.

\subsection{\Finished{SQL support}}

SQL Api an is important feature of data processing technologies, because it allows to use widely-known, very high-level
 language to define data flows. In table \ref{tab:dev-sql} we can see what are the options to use SQL language with
 different processing engines.

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Technology & SQL support \\
  \hline
  MapReduce  &  Apache Hive \& Apache Pig\\
  Spark      &  Spark SQL \\
  Flink      &  Table API (experimental) \\
  Samza      &  SamzaSQL (external project) \\
  Storm      &  Storm SQL \\
  Heron      &  None, because Storm SQL compiles to Trident topologies, which are not supported in Heron. \\
  Apex       &  malhar-sql \\
  Kafka Streams&  None \\
  Gearpump   &  None \\
  \hline
\end{tabular} \
\end{center}
\caption{\label{tab:dev-sql}SQL queries support in different technologies.}
\end{table}
\FloatBarrier

We can see most of the technologies has some kind of the support for SQL queries. It is important to mention that in
 case of \emph{Apache Spark}, one of the most important reasons to develop SQL API is to optimize users
  computations.

\subsection{\Finished{Machine learning support}}

Machine learning is a branch of data processing which is growing very fast nowadays. Is consist of many algorithms
crafted to allow creating the \emph{models} which are be able to solve certain problems without implementing specific
solution (e.g. classification or prediction). Such algorithms require a lot of computing power so it is useful to integrate
them with distributed processing engine instead of running them on single machine. In table \ref{tab:dev-ml} we can see
which of the technologies have support for some kind of machine learning algorithms/frameworks. Note that \emph{None}
 does not
mean that machine learning cannot be implemented on top of the given technology, but rather that there is no out-of-the-box solution.

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Technology & Machine learning support \\
  \hline
  MapReduce  &  Apache Mahout \\
  Spark      &  Spark MLlib \\
  Flink      &  FlinkML \\
  Samza      &  Apache SAMOA \\
  Storm      &  Apache SAMOA \\
  Heron      &  None \\
  Apex       &  Apache SAMOA \\
  Kafka Streams&  None \\
  Gearpump   &  None \\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-ml}Machine learning support in different technologies.}
\end{table}
\FloatBarrier

\subsection{\Finished{Notebooks support}}

Notebooks are web tool used by data scientists to create reports and work with data in general. In table
\ref{tab:dev-notebooks}  we see that only MapReduce, Flink and Spark have some kind of support. The reason behind
such  situation is that notebooks are most useful when dealing with batch processing and have very limited (or none)
usability when it comes to streaming. For MapReduce we can treat \emph{Apache Hue} as a notebook, but it allows user
to  create data processing based on SQL APIs rather than using raw MapReduce.

\FloatBarrier
\begin{table}[h!]
\begin{center}
\begin{tabular}{|p{3cm}|p{9cm}|} 
  \hline
  Technology & Notebook support \\
  \hline
  MapReduce  &  None/Hue (via Apache Hive/Pig) \\
  Spark      &  spark-notebook, Apache Zeppelin \\
  Flink      &  Apache Zeppelin \\
  Samza      &  None \\
  Storm      &  None \\
  Heron      &  None \\
  Apex       &  None \\
  Kafka Streams&  None \\
  Gearpump   &  None \\
  \hline
\end{tabular} 
\end{center}
\caption{\label{tab:dev-notebooks}Notebooks support in different technologies.}
\end{table}
\FloatBarrier


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
